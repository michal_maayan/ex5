import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class WritableFilter extends CreateFilter {
    private String writable;
    public WritableFilter(String isWritable, boolean hasNot) {
        super(hasNot);
        writable = isWritable;
    }

    /**
     *
     * @param file
     * @return
     */
    protected boolean shouldFilterChild(File file){
        if(writable.equals("YES")) {
            return file.canWrite();
        }
        // The wanted files are all the files which doesn't have write permission.
        return !file.canWrite();

    }
}
