import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public abstract class CreateFilter {
    public static final String NOT_SUFFIX = "#NOT";
    protected boolean hasNot = false;

    /**
     * Default constructor
     */
    public CreateFilter(boolean hasNot) {
        this.hasNot = hasNot;
    }

    /**
     * In case the filter ends with "NOT" return the negative of the filter
     * @param file the file object
     * @return true if the file needs to be filtered and false otherwise.
     */
    public boolean shouldFilter(File file){
        if (hasNot){
            return !shouldFilterChild(file);
        }
        return shouldFilterChild(file);
    }

    /**
     * //@todo מה כתוב פההה???
     * Check if a specific file fits the filter condition.
     * @param file the file object
     * @return true if the file needs to be filtered and false otherwise.
     */
    abstract boolean shouldFilterChild(File file);

    // the filterType isn't null
    static CreateFilter isValid(String filterType) throws Type1Exception {
        boolean hasNot = false;
        if(filterType.endsWith(NOT_SUFFIX)){
            hasNot = true;
            filterType = filterType.substring(0, filterType.length() - NOT_SUFFIX.length());
        }
        String[] filterArgs = filterType.split("#");
        CreateFilter filter;
        switch (filterArgs[0]) {
            case "all":
                if (filterArgs.length != 1) {
                    throw new Type1Exception("Bad length");
                }
                filter = new AllFilter(hasNot);
                return filter;
            case "prefix":
                //todo - michal - delete this if
//                if (filterArgs.length != 2) {
//                    throw new Type1Exception("Bad length");
//                }
                filter = new PrefixFilter(filterArgs[1],hasNot);
                return filter;
            case "suffix":
                //todo - michal - delete this if
//                if (filterArgs.length != 2) {
//                    throw new Type1Exception("Bad length");
//                }
                filter = new SuffixFilter(filterArgs[1],hasNot);
                return filter;
            case "contains":
                argsLength2(filterArgs.length);
                //todo - michal - delete this if
//                if (filterArgs.length != 2) {
//                    throw new Type1Exception("Bad length");
//                }
                filter = new ContainFilter(filterArgs[1],hasNot);
                return filter;
            // todo - michal - hpw to vlid the second parameter is STRING
            case "file":
                argsLength2(filterArgs.length);
                filter = new FileFilter(filterArgs[1],hasNot);
                return filter;
            case "writable":
                argsLength2(filterArgs.length);
                yesOrNo(filterArgs[1]);
                filter = new WritableFilter(filterArgs[1],hasNot);
                return filter;
            case "executable":
                argsLength2(filterArgs.length);
                yesOrNo(filterArgs[1]);
                filter = new ExecutableFilter(filterArgs[1],hasNot);
                return filter;
            case "hidden":
                argsLength2(filterArgs.length);
                yesOrNo(filterArgs[1]);
                filter = new HiddenFilter(filterArgs[1],hasNot);
                return filter;
            case "greater_than":
                argsLength2(filterArgs.length);
                isDoubleNonNegative(filterArgs[1]);
                filter = new GreaterThanFilter(filterArgs[1],hasNot);
                return filter;
            case "smaller_than":
                argsLength2(filterArgs.length);
                isDoubleNonNegative(filterArgs[1]);
                filter = new GreaterThanFilter(filterArgs[1],hasNot);
                return filter;
            case "between":
                if (filterArgs.length != 3) {
                    throw new Type1Exception("Bad length");
                }
                isDoubleNonNegative(filterArgs[1]);
                isDoubleNonNegative(filterArgs[2]);
                filter = new BetweenFilter(filterArgs[1],filterArgs[2],hasNot);
                return filter;
            default:
                throw new Type1Exception();
        }
    }

    private static void argsLength2 (int length) throws Type1Exception{
        if (length != 2) {
            throw new Type1Exception("Bad length");
        }
    }

    private static void yesOrNo (String strValidation) throws Type1Exception{
        if (!strValidation.equals("YES") || !strValidation.equals("NO")) {
            throw new Type1Exception("Bad parameters to the hidden/writable/executable filters");
        }
    }

    private static void isDoubleNonNegative (String strValidation) throws Type1Exception{
        if (Double.parseDouble(strValidation) < 0) {
            throw new Type1Exception("Double parameters is negative");
        }
    }

// I've used this part for tests

//    public static void main(String[] args) throws Type1exp {
//        try {
//            CreateFilter filter = isValid("prefix#a");
//            System.out.println(filter.shouldFilter("aaabbab"));
//        }
//        catch (Type1exp e){
//            System.out.println(e);
//        }
//
//    }

}
