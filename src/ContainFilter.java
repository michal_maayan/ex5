import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class ContainFilter extends CreateFilter {
    private String contain;
    public ContainFilter(String containval, boolean hasNot) {
        super(hasNot);
        //@todo - containVal
        contain = containval;
    }

    /**
     *
     * @param name
     * @return
     */
    protected boolean shouldFilterChild(File file){
        return file.getName().contains(contain);
    }
}
