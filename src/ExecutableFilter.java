import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class ExecutableFilter extends CreateFilter {
    private String executable;
    public ExecutableFilter(String isExecutable, boolean hasNot) {
        super(hasNot);
        executable = isExecutable;
    }

    /**
     *
     * @param file
     * @return
     */
    protected boolean shouldFilterChild(File file){
        if(executable.equals("YES")) {
            return file.canExecute();
        }
        // The wanted files are all the files which doesn't have execute permission.
        return !file.canExecute();
    }
}
