/**
 * Created by Steve on 19-May-17.
 */

import java.util.LinkedList;

/**
 * A class that creates a 4 line sections in a String array format.
 * arr[0] = "FILTER"
 * arr[1] = A specific filter (could be null)
 * arr[2] = "ORDER"
 * arr[3] = A specific order (could be null)
 *
 */
public class Section {
    TextLine[] arguments = new TextLine[4];
    int currentIndex;
    LinkedList<String> warnings = new LinkedList<String>();


    public Section(){}

    /**
     * Adding the current String to the array and update the index to the new empty
     * cell in the array
     * @param value needs to be insert to the array
     */
    void add(String value, int lineNum){
        arguments[currentIndex] = new TextLine(value, lineNum);
        currentIndex ++;
    }


    void addWarning(int line){
        this.warnings.add("Warning in line " + line);
    }

    void printWarnings(){
        for (String message : warnings){
            System.out.println(message);
        }
    }




}
