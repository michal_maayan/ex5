

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created by micim on 19-May-17.
 */

public class DirectoryProcessor {
    final String DEFAULT_FILTER = "all", DEFAULT_ORDER = "abs", FILTER = "FILTER", ORDER = "ORDER";
    LinkedList<Section> sections = new LinkedList<Section>();
    final int ORDER_ROW = 3, FILTER_ROW = 1;
    boolean DEFAULT_FILTER_SUFFIX = false;
    //parameter for the TextLine
    int lineNum = 0;

    public void parseFile (String[] args) throws Type2Exception {
        String filter = DEFAULT_FILTER;
        String order = DEFAULT_ORDER;
        try {
            File file = new File(args[1]);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // Reading the first line in the file
            String textLine = bufferedReader.readLine();

            // The while logic:
            // 1. If there is more data in the file, checks if it starts with "FILTER"
            // 2. Validate that the new section in the file starts with "FILTER"
            // 3. Create a new section obl and add the filter cell
            // 4. Inner while logic is described near the inner while.
            // 5. There is another "FILTER" line, which means a new section needs to be created.
            // 5.1 Adding the current section to the sections list (which is done
            //      before starting a new one, with the new "FILTER" argument)
            while (textLine != null) {
                // Counting line numbers in the file, increase whenever a "readLine()" command call
                lineNum++;
                // In case the new section in the file doesn't start with "FILTER"
                if (!(textLine.equals(FILTER))){
                    throw new Type2Exception("ERROR: FILTER sub-section missing");
                }

                // A new section object is generated
                Section curSection = new Section();
                curSection.add(FILTER,lineNum);

                // Inner-while logic:
                // 1. There is maximum 4 lines in a section and there is only 1 filter in a section.
                // 2. Validate that that a "FILTER" sub-section exists
                // 3. Validate that an "ORDER" section exists.
                while(curSection.currentIndex <= 3 && (textLine = bufferedReader.readLine()) != null && !textLine.equals(FILTER)) {
                    //counting line numbers in the file, increase whenever a "readLine()" command call
                    lineNum++;

                    // adding the "ORDER" string to the third cell in the array
                    if(curSection.currentIndex == 1 && textLine.equals(ORDER)){
                        throw new Type2Exception("ERROR: FILTER sub-section missing");
                    }

                    // In case there is no "ORDER" string in the file section
                    if (curSection.currentIndex == 2 && !textLine.equals(ORDER)){
                        throw new Type2Exception("ERROR: ORDER sub-section missing");
                    }
                    curSection.add(textLine,lineNum);

                }
                sections.add(curSection);
                // ends of the new section, meaning there is a section with 4 lines.
            }
        }
        catch (IOException e) {
            throw new Type2Exception("Invalid path");
        }
    }

    /**
     * Creata a list of all the files in the given directory which fits to the filter's conditions.
     * @param folder of all the files
     * @return a list of a filtered files.
     */
    public LinkedList listFilesForFolder(final File folder, CreateFilter filter) {
        LinkedList filesList = new LinkedList();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory() && filter.shouldFilter(fileEntry)) {
                filesList.add(fileEntry);
                System.out.println(fileEntry.getName());
            }
        }
        System.out.println("new section");
        return filesList;
    }


    public void printResults(String[] args){
        for(Section section : sections){
            Comparator<File> command;
            CreateFilter filter;
//            try {
//                command = Order.createComparator(section.arguments[ORDER_ROW].line);
//            }
//
//            // There was an invalid order command, so create the default order Comparator and add a warning
//            catch(Type1Exception e){
//                section.addWarning(section.arguments[ORDER_ROW].lineNum);
//                command = Order :: comparePaths;
//            }

            try{
                filter = CreateFilter.isValid(section.arguments[FILTER_ROW].line);
            }

            // There was an invalid Filter command, so create the default Filter and add a warning
            catch(Type1Exception e){
                section.addWarning(section.arguments[FILTER_ROW].lineNum);
                filter = new AllFilter(DEFAULT_FILTER_SUFFIX);
            }

            final File folder = new File(args[0]);
            // for tests
//            CreateFilter filter = new SuffixFilter("txt",false);
            LinkedList fileNamesList = listFilesForFolder(folder, filter);
//            for(Object name:fileNamesList){
//                System.out.println(name);
//            }


//            Stream a = fileNamesList.stream().sorted(command);

            // print the warnings for current section
            if (section.warnings.size() != 0){
                section.printWarnings();
            }

//            // print the relevant file names
//            for (Object fliName : fileNamesList) {
//                System.out.println(fliName);
//            }


        }
    }


//todo - Stav - do you we need it or should I delete it?
//    public void printResults(){
//        for(Section section : sections){
//            Comparator<File> command;
//            try {
//                command = Order.createComparator(section.arguments[ORDER_ROW]);
//            }
//            catch(Type1Exception e){
//                int lineNum = (section.serialNumber * ARGUMENT_ROW_NUM);
//                section.addWarning(lineNum);
//                command = Order :: comparePaths;
//            }
//
//
//
//
//        }
//    }






    public static void main(String[] args) throws Type2Exception{
        DirectoryProcessor a = new DirectoryProcessor();
        a.parseFile(args);
        a.printResults(args);
    }

}
