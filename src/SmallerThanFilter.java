import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class SmallerThanFilter extends CreateFilter {
    private double smaller;
    private final int CONVERT_TO_KB = 1024;
    public SmallerThanFilter(String smallerval, boolean hasNot) {
        super(hasNot);
        smaller = Double.parseDouble(smallerval);

    }

    /**
     *
     * @param file
     * @return
     */
    protected boolean shouldFilterChild(File file){
        return file.length() / CONVERT_TO_KB < smaller;
    }
}
