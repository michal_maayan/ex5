import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class BetweenFilter extends CreateFilter {
    private double greater, smaller;
    private final int CONVERT_TO_KB = 1024;
    public BetweenFilter(String greateral, String smallerval, boolean hasNot) {
        super(hasNot);
        greater = Double.parseDouble(greateral);
        smaller =  Double.parseDouble(smallerval);
    }

    /**
     *
     * @param file
     * @return
     */
    protected boolean shouldFilterChild(File file){
        return (file.length() / CONVERT_TO_KB > greater && file.length() / CONVERT_TO_KB < smaller);
    }
}
