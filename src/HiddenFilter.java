import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class HiddenFilter extends CreateFilter {
    private String hidden;
    public HiddenFilter(String isHidden, boolean hasNot) {
        super(hasNot);
        hidden = isHidden;
    }

    /**
     *
     * @param file
     * @return
     */
    protected boolean shouldFilterChild(File file){
        if(hidden.equals("YES")) {
            return file.isHidden();
        }
        // The wanted files are all the files which aren't hide.
        return !file.isHidden();
    }
}
