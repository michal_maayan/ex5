import java.util.Comparator;
import java.io.File;

/**
 * Created by Steve on 22-May-17.
 */
public class Order {

    static final String REVERSE = "#REVERSE";

    /**
     * receives an Order command and return a comparator that acts according to the command.
     * @param command the requested order
     * @return Comparator<File> that sorts accordind to the given command
     * @throws Type1Exception in case if invalid command
     */
    static Comparator<File> createComparator (String command) throws Type1Exception{
        boolean reverse = false;

        // Check if reversed order was requested
        if (command.endsWith(REVERSE)){
            reverse = true;
            int lastIndex = command.indexOf(REVERSE);
            command = command.substring(0, lastIndex);
        }
        Comparator<File> comparator;

        // Check which type of order was requested
        switch(command){
            case "abs":
                comparator = Order :: comparePaths;
                break;

            case "type":
                comparator = Order :: compareTypes;
                break;

            case "size":
                comparator = Order :: compareSizes;
                break;

            //else: invalid command
            default:
                throw new Type1Exception();
        }
        if(reverse){
            return comparator.reversed();
        }
        return comparator;
    }


    /**
     * Compares two files by their absolute path, lexicographically
     * @param f1 File number 1
     * @param f2 File number 2
     * @return the value 0 if the argument f2 is equal to f1; a value less than 0 if f2 is
     * lexicographically less than the f1; and a value greater than 0 if f2 is lexicographically
     * greater than f1.
     */
    static int comparePaths(File f1, File f2) {
        return f1.getAbsolutePath().compareTo(f2.getAbsolutePath());
    }


    /**
     * Compares between two Files according to their size.
     * @param f1 File number 1
     * @param f2 File number 2
     * @return the value 0 if f1 is equal to f2; a value less than 0 if f1 is numerically less than f2 Long;
     * and a value greater than 0 if f1 is numerically greater than f2.
     */
    static int compareSizes(File f1, File f2){
        Long size1 = f1.length();
        Long size2 = f2.length();
        return size1.compareTo(size2);
    }


    /**
     * Compares two files by their types path, lexicographically
     * @param f1 File number 1
     * @param f2 File number 2
     * @return the value 0 if the argument f2 is equal to f1; a value less than 0 if f2 is
     * lexicographically less than the f1; and a value greater than 0 if f2 is lexicographically
     * greater than f1.
     */
    static int compareTypes(File f1, File f2) {
        String type1 = getType(f1);
        String type2 = getType(f2);
        return type1.compareTo(type2);
    }


    /**
     * helper method that gets the type of a certain file, in String form.
     * @param file The above File.
     * @return String representing the File type.
     */
    static String getType(File file){
        String path = file.getName();
        int index = path.lastIndexOf(".");
        String type = path.substring( index + 1 );
        return type;
    }


}
