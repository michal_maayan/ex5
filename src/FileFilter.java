import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class FileFilter extends CreateFilter {
    private String file;
    public FileFilter(String fileval, boolean hasNot) {
        super(hasNot);
        //@todo - containVal
        this.file = fileval;
    }

    /**
     *
     * @param name
     * @return
     */
    protected boolean shouldFilterChild(File file){
        return file.getName().equals(file);
    }
}
