import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class SuffixFilter extends CreateFilter {
    private String suffix;
    public SuffixFilter(String suffixval, boolean hasNot) {
        super(hasNot);
        suffix = suffixval;
    }

    /**
     *
     * @param name
     * @return
     */
    protected boolean shouldFilterChild(File file){
        return file.getName().endsWith(suffix);
    }
}
