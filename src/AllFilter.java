import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class AllFilter extends CreateFilter {
    public AllFilter(boolean hasNot) {
        super(hasNot);

    }

    /**
     * All the files needs to be filtered
     * @param file the file obj
     * @return true because all the files are filtered
     */
    protected boolean shouldFilterChild(File file){
        return true;
    }
}
