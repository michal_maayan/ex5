import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class PrefixFilter extends CreateFilter {
    private String prefix;
    public PrefixFilter(String prefixval, boolean hasNot) {
        super(hasNot);
        prefix = prefixval;
    }

    protected boolean shouldFilterChild(File file){
        return (file.getName().startsWith(prefix));
    }
}
