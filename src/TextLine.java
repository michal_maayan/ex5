/**
 * Created by micim on 22-May-17.
 */

/**
 * Creates an object that has a String value and an int value, which represents a text line in a file,
 * and its line number in the file.
 */
public class TextLine {
    String line;
    int lineNum;
    public TextLine(String line, int lineNum){
        this.line = line;
        this.lineNum = lineNum;
    }
}
