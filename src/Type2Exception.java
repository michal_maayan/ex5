/**
 * Created by Steve on 19-May-17.
 */
public class Type2Exception extends Exception {
    Type2Exception(String message){
        super(message);
    }
}
