import java.io.File;

/**
 * Created by micim on 22-May-17.
 */
public class GreaterThanFilter extends CreateFilter {
    private double greater;
    private final int CONVERT_TO_KB = 1024;
    public GreaterThanFilter(String greateral, boolean hasNot) {
        super(hasNot);
        greater = Double.parseDouble(greateral);

    }

    /**
     *
     * @param file
     * @return
     */
    protected boolean shouldFilterChild(File file){
        return file.length() / CONVERT_TO_KB > greater;
    }
}
